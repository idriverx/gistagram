<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <center><h3>Добро пожаловать в Gistagram!</h3><hr>
    <?= Html::img(Yii::getAlias('@web') . '/images/iphone7.png', ['width' => '225px']) ?>
    <?= Html::img(Yii::getAlias('@web') . '/images/pc.png') ?></center>

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        
    ]); ?>

        <?= $form->field($model, 'login')->textInput() ?>

        <?= $form->field($model, 'password')->passwordInput() ?>


        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <center>
                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary']) ?>
                 <?= Html::a('Регистрация', ['auth/register'], ['class' => 'btn btn-info']) ?>
                </center>
            </div>
        </div>

    <?php ActiveForm::end(); ?>


</div>
